#!/bin/bash

for i in {1..10}
do
	`scp -v files/500 root@172.16.0.3:~/test/ >> results2/results_big 2>&1 &`
	
	sleep 1

	`./mice_generator.sh > results2/results_small_${i} 2>&1 &`

	wait

	sleep 60
done


