#!/bin/bash

files=()
total=0
size=100

for i in {0..19}
do
	let "file=($RANDOM%10)+1"
	files[$i]=$file
	total=$((total+file))
done

if [ $total -gt $size ]
then
	change=-1
elif [ $total -lt $size ]
then
	change=1
fi

if [ $total -ne $size ]
then
	while [ $total -ne $size ]
	do
		let "pos=($RANDOM%19)"
		if [[ $change -eq -1 && ${files[$pos]} -gt 1 ]] || [[ $change -eq 1 && ${files[$pos]} -lt 10 ]]
		then
			files[$pos]=$((files[$pos]+change))
			total=$((total+change))
		fi
	done
fi

for i in {0..19}
do
	`scp -v files/${files[$i]} root@172.16.0.3:~/test/ &`
	let "sleep=($RANDOM%5)+1"
	sleep $sleep
done
