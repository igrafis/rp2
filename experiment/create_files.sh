#!/bin/bash

`mkdir files`

for i in {1..10}
do
	`dd if=/dev/urandom of=files/$i bs=1024 count=0 seek=\$[1024*$i]`
done

`dd if=/dev/urandom of=files/500 bs=1024 count=0 seek=\$[1024*500]`
