package controller;

import java.util.HashMap;
import java.util.Map;

public class Switch {
	private final String switchDPID;
	private final String ip;
	private final long connectedSince;
	private final String ipInt;
	private int portInt;
	private Map<String,Link> links;
	private Map<String,Flow> flows;
	
	public Switch(String switchDPID, String ip, long connectedSince, String ipInt) {
		this.switchDPID = switchDPID;
		this.ip = ip;
		this.connectedSince = connectedSince;
		this.ipInt = ipInt;
		this.links = new HashMap<String, Link>();
		this.flows = new HashMap<String,Flow>();
	}
	
	public String getSwitchDPID() {
		return switchDPID;
	}

	public String getIp() {
		return ip;
	}
	
	public long getConnectedSince() {
		return connectedSince;
	}
	
	public String getIpInt() {
		return ipInt;
	}
	
	public void setPortInt(int portInt) {
		this.portInt = portInt;
	}
	
	public int getPortInt() {
		return this.portInt;
	}
	
	public void addLink(Link link) {
		this.links.put(link.getName(), link);
	}
	
	public Map<String,Link> getLinks() {
		return this.links;
	}
	
	public void addFlow(Flow flow) {
		this.flows.put(flow.getName(), flow);
	}
	
	public void emptyFlows() {
		this.flows.clear();
	}
	
	public Map<String,Flow> getFlows() {
		return this.flows;
	}

	@Override
	public String toString() {
		return "Switch [switchDPID=" + switchDPID + ", ip=" + ip + ", connectedSince=" + connectedSince + ", ipInt=" + ipInt + "]";
	}
}
