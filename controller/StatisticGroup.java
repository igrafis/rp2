package controller;

import java.util.HashMap;
import java.util.Map;

public class StatisticGroup {
	private final long timestamp;
	private Map<String, Statistic> statistics;
	
	public StatisticGroup(long timestamp) {
		this.timestamp = timestamp;
		this.statistics = new HashMap<String, Statistic>();
	}
	
	public long getTimestamp() {
		return this.timestamp;
	}
	
	public void addStatistics(Statistic statistic, String switchDPID) {
		this.statistics.put(switchDPID, statistic);
	}
	
	public void emptyStatistics() {
		this.statistics.clear();
	}
	
	public Map<String, Statistic> getStatistics() {
		return this.statistics;
	}
	
	@Override
	public String toString() {
		return "StatisticGroup [timestamp=" + timestamp + "]";
	}
}
