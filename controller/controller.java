package controller;

public class mainTest {

	public static void main(String[] args) {
		ControllerApi api = new ControllerApi();	
		
		// Check network
		try {
			while(true) {
				api.findTopology();
				api.findStatistics();
				api.checkCongestion();
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
