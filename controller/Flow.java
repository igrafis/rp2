package controller;

import java.util.ArrayList;
import java.util.List;

public class Flow {
	public int ELEPHANT = 2;
	public int MOUSE = 1;
	public int DEAD = 0;
	
	private final String name;
	private final String nameCouple;
	private final String ipSrc;
	private final String ipDst;
	private final int tcpSrc;
	private final int tcpDst;
	private int type;
	private String switchSrc;
	private String switchDst;
	private boolean useLongPath;
	private List<StatisticGroup> statistics;

	public Flow(String name, String nameCouple, String ipSrc, String ipDst, int tcpSrc, int tcpDst) {
		this.name = name;
		this.nameCouple = nameCouple;
		this.ipSrc = ipSrc;
		this.ipDst = ipDst;
		this.tcpSrc = tcpSrc;
		this.tcpDst = tcpDst;
		this.type = 1;
		this.switchSrc = "";
		this.switchDst = "";
		this.useLongPath = false;
		this.statistics = new ArrayList<StatisticGroup>();
	}
	
	public String getName() {
		return name;
	}
	
	public String getNameCouple() {
		return nameCouple;
	}
	
	public String getIpSrc() {
		return ipSrc;
	}

	public String getIpDst() {
		return ipDst;
	}
	
	public int getTcpSrc() {
		return tcpSrc;
	}

	public int getTcpDst() {
		return tcpDst;
	}
	
	public void addStatistics(Statistic statistic, long timestamp, String switchDPID) {
		if (this.statistics.size() > 0) {
			if (this.statistics.get(0).getTimestamp() != timestamp) {
				this.statistics.add(0, new StatisticGroup(timestamp));
			}
		} else {
			this.statistics.add(0, new StatisticGroup(timestamp));
		}
		
		this.statistics.get(0).addStatistics(statistic, switchDPID);
		
		int size = this.statistics.size() - 1;
		if (size >= 10)
			this.statistics.remove(size);
	}
	
	public void emptyStatistics() {
		this.statistics.clear();
	}
	
	public List<StatisticGroup> getStatistics() {
		return this.statistics;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public String getSwitchSrc() {
		return this.switchSrc;
	}
	
	public void setSwitchSrc(String switchSrc) {
		this.switchSrc = switchSrc;
	}
	
	public String getSwitchDst() {
		return this.switchDst;
	}
	
	public void setSwitchDst(String switchDst) {
		this.switchDst = switchDst;
	}
	
	public boolean getUseLongPath() {
		return useLongPath;
	}
	
	public void setUseLongPath(boolean useLongPath) {
		this.useLongPath = useLongPath;
	}
	
	public String flowName() {
        return ipSrc + "-" + ipDst + "-" + Integer.toString(tcpSrc) + "-" + Integer.toString(tcpDst);
	}

	@Override
	public String toString() {
		return "Flow [name=" + name + ", ipSrc=" + ipSrc + ", ipDst=" + ipDst + ", tcpSrc=" + tcpSrc + ", tcpDst=" + tcpDst + "]";
	}
}
