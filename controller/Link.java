package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Link {
	private final String name;
	private final int dst_port;
	private final Switch dst_switch;
	private final int src_port;
	private final Switch src_switch;
	private List<StatisticGroup> statistics;
	private Map<String,Flow> flows;

	public Link(String name, Integer dst_port, Switch dst_switch, Integer src_port, Switch src_switch) {
		this.name = name;
		this.dst_port = dst_port;
		this.dst_switch = dst_switch;
		this.src_port = src_port;
		this.src_switch = src_switch;
		this.statistics = new ArrayList<StatisticGroup>();
		this.flows = new HashMap<String,Flow>();
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getDstPort() {
		return dst_port;
	}

	public Switch getDstSwitch() {
		return dst_switch;
	}
	
	public int getSrcPort() {
		return src_port;
	}

	public Switch getSrcSwitch() {
		return src_switch;
	}
	
	public void addStatistics(Statistic statistic, long timestamp, String switchDPID) {
		if (this.statistics.size() > 0) {
			if (this.statistics.get(0).getTimestamp() != timestamp) {
				this.statistics.add(0, new StatisticGroup(timestamp));
			}
		} else {
			this.statistics.add(0, new StatisticGroup(timestamp));
		}
		
		this.statistics.get(0).addStatistics(statistic, switchDPID);
		
		int size = this.statistics.size() - 1;
		if (size >= 10)
			this.statistics.remove(size);
	}
	
	public void emptyStatistics() {
		this.statistics.clear();
	}
	
	public List<StatisticGroup> getStatistics() {
		return this.statistics;
	}
	
	public void addFlow(Flow flow) {
		this.flows.put(flow.getName(), flow);
	}
	
	public void emptyFlows() {
		this.flows.clear();
	}
	
	public Map<String,Flow> getFlows() {
		return this.flows;
	}

	@Override
	public String toString() {
		return "Link [dst_port=" + dst_port + ", dst_switch=" + dst_switch.getSwitchDPID() + ", src_port=" + src_port + ", src_switch=" + src_switch.getSwitchDPID() + "]";
	}
}
