package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import nl.tno.computefactory.dns.DnsApi;
import nl.tno.computefactory.dns.Record;
import nl.tno.computefactory.util.CFConfig;
import nl.tno.computefactory.util.CFUtil;

import org.jgrapht.GraphPath;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.KShortestPaths;
import org.jgrapht.graph.Pseudograph;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ControllerApi {
	private final int CHECK_DISTANCE = 5;
	private final int ELEPHANT_SIZE = 10;
	private final long THRESHOLD = 20 * 1000 * 1000;
	private final int K_SHORTEST_PATHS = 4;
	
	private final CFConfig cfConfig = new CFConfig();
	private final Logger logger = LoggerFactory.getLogger(ControllerApi.class);
	private final DnsApi dns = new DnsApi();
	
	private String controller_ip = cfConfig.OPENFLOW_CONTROLLER;

	private Map<String,Switch> switches = new HashMap<String, Switch>();
	private Map<String,Link> links = new HashMap<String, Link>();
	private Map<String,Path> paths = new HashMap<String, Path>();
	private Map<String,Flow> flows = new HashMap<String, Flow>();
	
	
	// Topology
	
	public void findTopology() throws Exception {
		boolean change1 = findSwitches();
		boolean change2 = findlinks();
		if (change1 || change2)
			caclulatePaths();
	}
	
	private boolean findSwitches() throws Exception {
		logger.trace("Find switches");
		
		String result = executeCommand("/wm/core/controller/switches/json");
		List<String> exist = new ArrayList<String>();
		JSONArray jsonArray = new JSONArray(result);
		boolean change = false;
		
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			String switchDPID = jsonObject.getString("switchDPID");
			
			exist.add(switchDPID);
			
			if (this.switches.containsKey(switchDPID))
				continue;
			
			String[] ips = jsonObject.getString("inetAddress").substring(1).split(":");
			String ip = ips[0];
			long connectedSince = Long.valueOf(jsonObject.getString("connectedSince")).longValue();
			
			String hostname = null, ipInt = null;
			
			List<Record> records = dns.listRecords();
			for (Record record1 : records) {
				if ((record1.getIpAddress()).equals(ip)) {
					hostname = record1.getHostname() + cfConfig.DNS_INTERNAL_APPEND;
					for (Record record2 : records) {
						if ((record2.getHostname()).equals(hostname) && !record2.getIpAddress().isEmpty()) {
							ipInt = record2.getIpAddress();
							break;
						}
					}
					break;
				}
			}
			
			Switch sw = new Switch(jsonObject.getString("switchDPID"), ip, connectedSince, ipInt);
			
			this.switches.put(switchDPID, sw);
			findIntPortSwitch(switchDPID);
			change = true;
			System.out.println("Switch added " + switchDPID);
		}
		
		for (Iterator<Map.Entry<String, Switch>> it = this.switches.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Switch> entry = it.next();
		    String key = entry.getKey();
		    if (!exist.contains(key)) {
		    	it.remove();
		    	change = true;
				System.out.println("Switch removed " + key);
			}
		}
		
		return change;
	}
	
	private void findIntPortSwitch(String switchDPID) throws Exception {
		String result = executeCommand("/wm/core/switch/" + switchDPID + "/port-desc/json");
		
		JSONObject jsonObjects = new JSONObject(result);
		JSONArray array_flows = jsonObjects.getJSONArray("portDesc");
		for (int j = 0; j < array_flows.length(); j++) {
			JSONObject jsonPort = array_flows.getJSONObject(j);
			
			String name = (String) jsonPort.get("name");
			if (!name.contains("loc"))
				continue;
			
			this.switches.get(switchDPID).setPortInt( jsonPort.getInt("portNumber"));
		}
	}
	
	private boolean findlinks() throws Exception {
		logger.trace("Find links");
		
		String result = executeCommand("/wm/topology/links/json");
		
		List<String> exist = new ArrayList<String>();
		JSONArray jsonArray = new JSONArray(result);
		String src_switch, dst_switch, name;
		Integer src_port, dst_port;
		Link link;
		Switch src, dst;
		boolean change = false;
		
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			dst_port = jsonObject.getInt("dst-port");
			dst_switch = jsonObject.getString("dst-switch");
			src_port = jsonObject.getInt("src-port");
			src_switch = jsonObject.getString("src-switch");
			
			name = createLinkName(src_switch, src_port, dst_switch, dst_port);
			
			exist.add(name);
			
			if (this.links.containsKey(name))
				continue;
			
			dst = this.switches.get(dst_switch);
			src = this.switches.get(src_switch);
			link = new Link(name, dst_port, dst, src_port, src);
			this.links.put(name, link);
			dst.addLink(link);
			src.addLink(link);
			change = true;
			System.out.println("Link added " + name);
		}
		
		for (Iterator<Map.Entry<String, Link>> it = this.links.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Link> entry = it.next();
		    String key = entry.getKey();
		    if (!exist.contains(key)) {
		    	it.remove();
		    	change = true;
				System.out.println("Link removed " + key);
			}
		}
		
		return change;
	}
	
	private void caclulatePaths() {
		logger.trace("Caclulate paths");
		
		UndirectedGraph<String, Link> graph = new Pseudograph<String, Link>(Link.class);
		List<String> exist = new ArrayList<String>();
		
		for (String key_switch : this.switches.keySet()) {
    		graph.addVertex(key_switch);
    	}
		for (String key_link : this.links.keySet()) {
			Link link = this.links.get(key_link);
			graph.addEdge(link.getSrcSwitch().getSwitchDPID(), link.getDstSwitch().getSwitchDPID(), link);
		}
		
		for (String from : this.switches.keySet()) {
			for (String to : this.switches.keySet()) {
				if (from.equals(to))
					continue;
				
				KShortestPaths<String, Link> shortest = new KShortestPaths<String, Link>(graph, from, K_SHORTEST_PATHS);
			    List<GraphPath<String, Link>> listLinks = shortest.getPaths(to);
			    
			    if (listLinks == null)
			    	continue;
			    
			    String name = createPathName(from, to);
			    exist.add(name);
			    
			    if (this.paths.containsKey(name)) {
			    	this.paths.get(name).SetListLinks(listLinks);
					continue;
			    }
			    
			    Path path = new Path(name, this.switches.get(from), this.switches.get(to), listLinks);
			    this.paths.put(name, path);
				
				System.out.println("Path added " + name + " " + listLinks.size());
			}
		}
		
		for (Iterator<Map.Entry<String, Path>> it = this.paths.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Path> entry = it.next();
		    String key = entry.getKey();
		    if (!exist.contains(key)) {
		    	it.remove();
				System.out.println("Path removed " + key);
			}
		}
	}
	
	// Statistics
	
	public void findStatistics() throws Exception {
		logger.trace("Find statistics");
		
		long timestamp = System.currentTimeMillis();
		for (String key_sw : this.switches.keySet()) {
			findPortStatistics(key_sw, timestamp);
		}
		findFlowStatistics(timestamp);
    }
	
	private void findPortStatistics(String switchDPID, long timestamp) throws Exception {
		logger.trace("Find port statistics {}", switchDPID);
		
		String result = executeCommand("/wm/core/switch/" + switchDPID + "/port/json");
		
		JSONObject jsonObjects = new JSONObject(result);
		JSONArray arrayPorts = jsonObjects.getJSONArray("port");
		for (int i = 0; i < arrayPorts.length(); i++) {
			JSONObject portStat = arrayPorts.getJSONObject(i);
			
			String portNumberString = portStat.getString("portNumber");
			if (portNumberString.equals("local"))
				continue;
			
			int portNumber = Integer.parseInt(portNumberString);
			
			String linkName = null;
			for (String key_link : this.switches.get(switchDPID).getLinks().keySet()) {
				if (key_link.contains(switchDPID + "-" + portNumber)) {
					linkName = key_link;
				}
			}
			
			if (linkName == null)
				continue;
			
			long byteCount = portStat.getLong("transmitBytes") + portStat.getLong("receiveBytes");
			Statistic statistic = new Statistic(byteCount, portStat.getLong("durationSec"));
			
			Link link = this.links.get(linkName);
			link.addStatistics(statistic, timestamp, switchDPID);
		}
	}
	
	private void findFlowStatistics(long timestamp) throws Exception {
		logger.trace("Find flow statistics");
		
		List<String> exist_global = new ArrayList<String>();
		for (String switchDPID : this.switches.keySet()) {
			String result = executeCommand("/wm/core/switch/" + switchDPID + "/flow/json");
	
			Switch sw = this.switches.get(switchDPID);
			
			for (String linkName : sw.getLinks().keySet()) {
				sw.getLinks().get(linkName).emptyFlows();
			}
			
			List<String> exist = new ArrayList<String>();
			JSONObject jsonObject = new JSONObject(result);
			JSONArray array_flows = jsonObject.getJSONArray("flows");
			for (int j = 0; j < array_flows.length(); j++) {
				JSONObject flow = array_flows.getJSONObject(j);
				JSONObject match = (JSONObject) flow.get("match");
				if (match.length() == 0)
					continue;
					
				if (!match.has("ipv4_src") || !match.has("ipv4_dst")) {
					continue;
				}
				
				String ipSrc = match.getString("ipv4_src");
				String ipDst = match.getString("ipv4_dst");
				int inPort = match.getInt("in_port");
				JSONObject instructions = flow.getJSONObject("instructions");
				JSONObject instruction_apply_actions = instructions.getJSONObject("instruction_apply_actions");
				int outPort = instruction_apply_actions.getInt("output");
				
				int tcpSrc = 0, tcpDst = 0;
				if (match.has("tcp_src"))
					tcpSrc = match.getInt("tcp_src");
				if (match.has("tcp_dst"))
					tcpDst = match.getInt("tcp_dst");
				
				long byteCount = Long.valueOf(flow.getString("byteCount")).longValue();
				long durationSeconds = Long.valueOf(flow.getString("durationSeconds")).longValue();
				
				String name = createFlowName(ipSrc, tcpSrc, ipDst, tcpDst);
				
				if (!this.flows.containsKey(name)) {
					String nameCouple = createFlowName(ipDst, tcpDst, ipSrc, tcpSrc);
					Flow fl = new Flow(name, nameCouple, ipSrc, ipDst, tcpSrc, tcpDst);
					this.flows.put(name, fl);
					System.out.println("Flow added " + name + " ");
				}
				
				for (String linkName : sw.getLinks().keySet()) {
					if (linkName.contains(switchDPID + "-" + inPort) || linkName.contains(switchDPID + "-" + outPort)) {
						this.links.get(linkName).addFlow(this.flows.get(name));
					}
				}
				
				this.flows.get(name).addStatistics(new Statistic(byteCount, durationSeconds), timestamp, switchDPID);
				exist.add(name);
				exist_global.add(name);
			}
		}
		
		for (Iterator<Map.Entry<String, Flow>> it = this.flows.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Flow> entry = it.next();
		    String key = entry.getKey();
		    if (!exist_global.contains(key)) {
		    	it.remove();
				System.out.println("Flow removed " + key + " ");
			}
		}
	}
	
	
	// Check congestion
	
	public void checkCongestion() throws Exception {
		logger.trace("Check congestion");
		
		Set<String> fullLinks = checkLinks();
		if (fullLinks.size() > 0) {
			categorizeFlows();
			checkSolutions(fullLinks);
		}
	}
	
	private Set<String> checkLinks() {
		logger.trace("Check links");
		
		Set<String> fullLinks = new HashSet<String>();
		long threshold = THRESHOLD;
		int position = CHECK_DISTANCE -1;
		
		for (Entry<String, Link> key_link : this.links.entrySet()) {
			Link link = key_link.getValue();
			
			List<StatisticGroup> statistics = link.getStatistics();
			if (statistics.size() < CHECK_DISTANCE)
				continue;
			
			long bits1 = 0, bits2 = 0, time1 = 0, time2 = 0;
			for (String key : statistics.get(0).getStatistics().keySet()) {
				bits1 += statistics.get(0).getStatistics().get(key).getByteCount();
				time1 += statistics.get(0).getStatistics().get(key).getDurationSeconds();
			}
			bits1 = bits1 / statistics.get(0).getStatistics().size() * 8;
			time1 = time1 / statistics.get(0).getStatistics().size();
			for (String key : statistics.get(position).getStatistics().keySet()) {
				bits2 += statistics.get(position).getStatistics().get(key).getByteCount();
				time2 += statistics.get(position).getStatistics().get(key).getDurationSeconds();
			}
			bits2 = bits2 / statistics.get(position).getStatistics().size() * 8;
			time2 = time2 / statistics.get(position).getStatistics().size();

			if (((bits1 - bits2) / (time1 - time2)) > threshold)
				fullLinks.add(link.getName());
		}
		
		return fullLinks;
	}
	
	private void categorizeFlows() {
		logger.trace("Categorize flows");
		
		Set<String> checked = new HashSet<String>();
		for (String linkName : this.links.keySet()) {
			Link link = this.links.get(linkName);
			int size =  link.getFlows().size();
			for (String key_flow : link.getFlows().keySet()) {
				if (checked.contains(key_flow))
					continue;
				
				Flow flow = this.flows.get(key_flow);
				
				checked.add(key_flow);
				
				if (flow.getType() == flow.ELEPHANT)
					continue;
				
				List<StatisticGroup> statistics = flow.getStatistics();
				if (statistics.size() < CHECK_DISTANCE)
					continue;
				
				long threshold = THRESHOLD / size;
				int position = CHECK_DISTANCE -1;
				
				long bits1 = 0, bits2 = 0, time1 = 0, time2 = 0;
				for (String key : statistics.get(0).getStatistics().keySet()) {
					bits1 += statistics.get(0).getStatistics().get(key).getByteCount();
					time1 += statistics.get(0).getStatistics().get(key).getDurationSeconds();
				}
				bits1 = bits1 / statistics.get(0).getStatistics().size() * 8;
				time1 = time1 / statistics.get(0).getStatistics().size();
				for (String key : statistics.get(position).getStatistics().keySet()) {
					bits2 += statistics.get(position).getStatistics().get(key).getByteCount();
					time2 += statistics.get(position).getStatistics().get(key).getDurationSeconds();
				}
				bits2 = bits2 / statistics.get(position).getStatistics().size() * 8;
				time2 = time2 / statistics.get(position).getStatistics().size();
				

				if ((((bits1 - bits2) / (time1 - time2)) > threshold) && (time1 > ELEPHANT_SIZE)) {
					flow.setType(flow.ELEPHANT);
					this.flows.get(flow.getNameCouple()).setType(flow.ELEPHANT);
				} else if ((bits1 - bits2) < 1000) {
					flow.setType(flow.DEAD);
					this.flows.get(flow.getNameCouple()).setType(flow.DEAD);
				}
			}
		}
	}
	
	private void checkSolutions(Set<String> fullLinks) throws Exception {
		logger.trace("Check solutions");
		
		Set<String> checked1 = new HashSet<String>();
		Set<String> checked2 = new HashSet<String>();
		for (String fullLink : fullLinks) {
			System.out.println("Check solutions " + fullLink);
			
			Link link = this.links.get(fullLink);
			
			if (link.getFlows().size() <= 2)
				continue;
			
			int elephants = 0, mise = 0;
			for (String key_flow : link.getFlows().keySet()) {
				if (checked1.contains(key_flow))
					continue;
				
				Flow flow1 = this.flows.get(key_flow);
				Flow flow2 = this.flows.get(flow1.getNameCouple());
				if ((flow1.getType() == flow1.ELEPHANT) || (flow2.getType() == flow1.ELEPHANT)) {
					elephants++;
				} else if ((flow1.getType() == flow1.MOUSE) || (flow2.getType() == flow1.MOUSE)) {
					mise++;
				}
				
				checked1.add(key_flow);
				checked1.add(flow2.getName());
			}
			
			for (String key_flow : link.getFlows().keySet()) {
				Flow flow = this.flows.get(key_flow);
				
				if (checked2.contains(key_flow))
					continue;
				
				checked2.add(key_flow);
				checked2.add(flow.getNameCouple());
				
				if (flow.getType() != flow.ELEPHANT)
					continue;
				
				
				if (mise == 0 && elephants > 1) {
					System.out.println("Load balance elephants");
					
					findFlowSwitches(flow);
					List<Link> otherPath = findOtherPath(link, flow);
					if (otherPath == null) {
						System.out.println("No solutions found");
					} else {
						System.out.println("ok");
						
						int maxElephants = 0;
						for (Link newLink : otherPath) {
							int linkElephants = 0;
							for (String flowName : newLink.getFlows().keySet()) {
								Flow checkFlow = this.flows.get(flowName);
								if (checkFlow.getType() == checkFlow.ELEPHANT) {
									linkElephants++;
								}
							}
							if (linkElephants > maxElephants)
								maxElephants = linkElephants;
						}
						
						if (elephants > maxElephants) {
							elephants--;
							applySolution(flow, otherPath);
						}
					}
				} else if (mise > 0 && elephants > 0) {
					System.out.println("Redirect elephants");
					findFlowSwitches(flow);
					List<Link> otherPath = findOtherPath(link, flow);
					if (otherPath == null) {
						System.out.println("No solutions found");
					} else {
						System.out.println("ok");
						applySolution(flow, otherPath);
					}
				}
			}
		}
	}
	
	private void findFlowSwitches(Flow flow) {
		logger.trace("Find flow switches");
		
		if (!flow.getSwitchSrc().isEmpty())
			return;
		
		for (String key_sw : this.switches.keySet()) {
			String ipInt = this.switches.get(key_sw).getIpInt();
			if (ipInt.equals(flow.getIpSrc())) {
				flow.setSwitchSrc(key_sw);
				this.flows.get(flow.getNameCouple()).setSwitchDst(key_sw);
			} else if (ipInt.equals(flow.getIpDst())) {
				flow.setSwitchDst(key_sw);
				this.flows.get(flow.getNameCouple()).setSwitchSrc(key_sw);
			}
		}
	}
	
	private List<Link> findOtherPath(Link link, Flow flow) {
		logger.trace("Check other paths");
		
		String pathName = createPathName(flow.getSwitchSrc(), flow.getSwitchDst());
		Path path = this.paths.get(pathName);
		for (GraphPath<String, Link> otherPath : path.getListLinks()) {
			if (otherPath.getEdgeList().contains(link))
				continue;
			
			boolean goodPath = true;
			for (Link newLink : otherPath.getEdgeList()) {
				for (String flowName : newLink.getFlows().keySet()) {
					Flow checkFlow = this.flows.get(flowName);
					if (checkFlow.getType() == checkFlow.MOUSE) {
						goodPath = false;
						break;
					}
				}
			}
			
			if (goodPath)
				return otherPath.getEdgeList();
		}
		return null;
	}
	
	private void applySolution(Flow flow, List<Link> otherPath) throws Exception {
		logger.trace("Apply solution");
		
		Flow flowCouple = this.flows.get(flow.getNameCouple());
		
		List<String> switchList = buildNodePath(otherPath, flow.getSwitchSrc(), otherPath.size());
		int i = 0;
		for (String sw : switchList) {
			int inPort = 0, outPort = 0;
			
			if (i < (switchList.size()-1)) {
				Link link = otherPath.get(i);
				if (link.getSrcSwitch().getSwitchDPID().equals(sw)) {
					outPort = link.getSrcPort();
				} else {
					outPort = link.getDstPort();
				}
			}
			
			if (i > 0) {
				Link link = otherPath.get((i-1));
				if (link.getSrcSwitch().getSwitchDPID().equals(sw)) {
					inPort = link.getSrcPort();
				} else {
					inPort = link.getDstPort();
				}
			}
			
			if (i == 0) {
				inPort = this.switches.get(sw).getPortInt();
			} else if (i == (switchList.size()-1)) {
				outPort = this.switches.get(sw).getPortInt();
			}
			
			pushFlow(sw, flow, inPort, outPort);
			pushFlow(sw, flowCouple, outPort, inPort);
			i++;
		}
	}
	
	private List<String> buildNodePath(List<Link> links, String start, int size) {
    	List<String> node_path = new ArrayList<String>();
    	String next;
    	
    	if (links.get(0).getSrcSwitch().getSwitchDPID().equals(start)) {
    		next = links.get(0).getDstSwitch().getSwitchDPID();
		} else {
			next = links.get(0).getSrcSwitch().getSwitchDPID();
		}
    	
    	if (links.size() > 1) {
    		node_path = buildNodePath(links.subList(1, links.size()), next, size);
    	}
    	node_path.add(0, next);
    	
    	if (links.size() == size) {
    		node_path.add(0, start);
    	}
    	
    	return node_path;
    }
	
	private void pushFlow(String sw, Flow flow, int inPort, int outPort) throws Exception {
		System.out.println("Push flow " + sw);
		
		String command = "/wm/staticflowpusher/json";
		JSONObject post = new JSONObject();
		
		post.put("switch", sw);
		post.put("active", "true");
		post.put("priority", "100");
		post.put("name", sw + "-" + flow.flowName());
		post.put("idle_timeout", "5");
		post.put("in_port", inPort);
		post.put("eth_type", "0x0800");
		post.put("ipv4_src", flow.getIpSrc());
		post.put("ipv4_dst", flow.getIpDst());
		if (flow.getTcpSrc() != 0)
			post.put("ip_proto", "0x06");
			post.put("tcp_src", Integer.toString(flow.getTcpSrc()));
		if (flow.getTcpDst() != 0)
			post.put("tcp_dst", Integer.toString(flow.getTcpDst()));
		post.put("instruction_apply_actions", "output=" + outPort);
		
		executePostCommand(command, post.toString());
	}
	
	
	// General functions

	private void executePostCommand(String command, String post) throws Exception {
		String url = controller_ip + command;
		logger.trace("Executing {} with {}", url, post);
		
		CFUtil.executeProgramWithoutOutput("curl", "-d " + post, url);
	}
	
	private String executeCommand(String command) throws Exception {
		String url = controller_ip + command;
		logger.trace("Executing {}", url);
		
		List<String> result = CFUtil.executeProgramWithOutput("curl", "-s", url);
		
		return result.get(0);
	}
	
	private String createLinkName(String src_switch, int src_port, String dst_switch, int dst_port) {
		return src_switch + "-" + src_port +  "-" + dst_switch +  "-" + dst_port;
	}
	
	private String createPathName(String src_switch, String dst_switch) {
		return src_switch +  "-" + dst_switch;
	}
	
	private String createFlowName(String ipSrc, int tcpSrc, String ipDst, int tcpDst) {
		return ipSrc + "-" + tcpSrc +  "-" + ipDst +  "-" + tcpDst;
	}
}
