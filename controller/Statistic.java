package controller;

public class Statistic {
	private final long byteCount;
	private final long durationSeconds;

	public Statistic(long byteCount, long durationSeconds) {
		this.byteCount = byteCount;
		this.durationSeconds = durationSeconds;
	}
	
	public long getByteCount() {
		return byteCount;
	}

	public long getDurationSeconds() {
		return durationSeconds;
	}
	
	@Override
	public String toString() {
		return "Statistic [byteCount=" + byteCount + ", durationSeconds=" + durationSeconds + "]";
	}
}
