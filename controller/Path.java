package controller;

import java.util.List;

import org.jgrapht.GraphPath;

public class Path {
	private final String name;
	private final Switch from;
	private final Switch to;
	List<GraphPath<String, Link>> listLinks;
	
	public Path(String name, Switch from, Switch to, List<GraphPath<String, Link>> listLinks) {
		this.name = name;
		this.from = from;
		this.to = to;
		this.listLinks = listLinks;
	}
	
	public void SetListLinks(List<GraphPath<String, Link>> listLinks) {
		this.listLinks = listLinks;
	}
	
	public List<GraphPath<String, Link>> getListLinks() {
		return this.listLinks;
	}
	
	@Override
	public String toString() {
		return "Path [name=" + name + ", from=" + from.getSwitchDPID() + ", to=" + to.getSwitchDPID() + "]";
	}
}
